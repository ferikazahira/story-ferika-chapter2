from django.test import TestCase
from .views import story7
from django.test import Client 
from django.urls import resolve, reverse
from .apps import Story7Config
from django.apps import apps

# Create your tests here.

class TestApps(TestCase):
    def setUp(self):
        self.client = Client()

    def test_url_story7_ada(self):
        response = Client().get('/story7/')
        self.assertEquals(response.status_code, 404)

class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.story7 = reverse("story7:story7")

    def test_views_story7(self):
        response = self.client.get(self.story7)
        self.assertTemplateUsed(response, 'story7/story7.html')
