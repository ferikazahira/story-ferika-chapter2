from django.test import TestCase
from django.test import Client 
from django.urls import resolve, reverse
from django.contrib.auth.models import User
from .models import Profile
from .forms import LoginForm, RegisterForm
from .views import login_page, signup_page, logout_page
from django.apps import apps
from .apps import Story9Config


class FormTest(TestCase):
    def test_form_is_valid(self):
        form_login = LoginForm(data={
            "username": "Kucing",
            "password": "123",
        })

        form_regist = RegisterForm(data={
            "username": "Kucing",
            "email": "kucing@gmail.com",
            "password_first" : "123",
            "password_again" : "123",
        })
        self.assertTrue(form_regist.is_valid())

    def test_form_invalid(self):
        form_login = LoginForm(data={})
        self.assertFalse(form_login.is_valid())
        form_register = RegisterForm(data={})
        self.assertFalse(form_register.is_valid())

    def test_form_regist_is_exist(self):
        form_regist = RegisterForm(data={
            "username": "Kucing",
            "email": "kucing@gmail.com",
            "password_first" : "123",
            "password_again" : "123",
        })
        form_regist = RegisterForm(data={
            "username": "Kucing",
            "email": "kucing@gmail.com",
            "password_first" : "123",
            "password_again" : "123",
        })
        self.assertTrue(form_regist.is_valid())



class UrlsTest(TestCase):
    def setUp(self):
        self.client = Client()

    def test_url_login_ada(self):
        response = Client().get('/halo/login')
        self.assertEquals(response.status_code, 200)
    
    def test_url_signup_ada(self):
        response = Client().get('/halo/signup')
        self.assertEquals(response.status_code, 200)
    
    def test_url_logout_ada(self):
        response = Client().get('/halo/logout')
        self.assertEquals(response.status_code, 404)

class ViewsTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.login_page = reverse("story9:login")
        self.register_page = reverse("story9:signup")
        self.logout_page = reverse("story9:logout")
        self.user_new = User.objects.create_user("Kucing","kucing@gmail.com", password='123')
        self.user_new.save()
        self.profile = Profile.objects.create(user=self.user_new)

    def test_GET_login(self):
        response = self.client.get(self.login_page, {
            'username': 'Kucing', 
            'password':'123'}, follow = True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story9/login.html')

    def test_GET_signup(self):
        response = self.client.get(self.register_page, {
            'username': 'Ferika', 
            'email': 'kucing@gmail.com', 
            'password_first':'123', 
            'password_again':'123'}, follow = True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story9/signup.html')
    
    def test_GET_logout(self):
        response = self.client.get(self.logout_page)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'main/home.html')

    def test_register_login_post(self):
        #register
        response = self.client.post(self.register_page, data = {
            "username" : "Kucing",
            "email" : "kucing@gmail.com",
            "password_first" : "123",
            "password_again" : "123",
        })

        response = self.client.post(self.register_page, data = {
            "username" : "Kucing",
            "email" : "kucing@gmail.com",
            "password_first" : "123",
            "password_again" : "12",
        })

        response = self.client.post(self.register_page, data = {
            "username" : "Kucingagain",
            "email" : "kucing123@gmail.com",
            "password_first" : "123",
            "password_again" : "123",
        })

        response = self.client.post(self.login_page,data ={
            "username" : "Kucing",
            "password" : "123"
        })
        self.assertEquals(response.status_code,302)

    def test_not_register_yet(self):
        response = self.client.post(self.login_page,data ={
            "username" : "Kucinglah",
            "password" : "123"
        })
        self.assertEquals(response.status_code,200)

        
class ModelTest(TestCase):
    def setUp(self):
        self.new_user = User.objects.create_user("Kucing", password="123")
        self.new_user.save()
        self.profile = Profile.objects.create(
            user = self.new_user
        )
        self.response = self.client.login(
            username = "Kucing",
            password = "123"
        )

    def test_instance_created(self):
        self.assertEqual(Profile.objects.count(),1)

    def test_instance_is_correct(self):
        self.assertEqual(Profile.objects.first().user,self.new_user)

    def test_to_string(self):
        self.assertIn("Kucing",str(self.new_user.profile))
