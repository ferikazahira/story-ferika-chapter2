$(document).ready(function() {
    $('#search').val('')
    $.ajax({
        method: 'GET',
        // url: 'data?q=percy+jackson',
        url: "https://www.googleapis.com/books/v1/volumes?q=harry+potter",
        success: function (response) {
            let bookList = $('tbody')
            bookList.empty()
            let rakBuku = response.items
            
            for (let i = 0; i < response.items.length; i++) {
                let book = rakBuku[i].volumeInfo

                if ('imageLinks' in book == false)
                    var img = $('<td>').text("-");
                else {
                    if ('smallThumbnail' in book.imageLinks == false)
                        var img = $('<td>').append($('<img>').attr({
                            'src': book.imageLinks.thumbnail
                        }));
                    else
                        var img = $('<td>').append($('<img>').attr({
                            'src': book.imageLinks.smallThumbnail
                        }));
                }
                var name = $('<td>').text(book.title);

                if ('authors' in book == false) var authors = $('<td>').text("-");
                else var authors = $('<td>').text(book.authors);

                var tr = $('<tr>').append(img, name, authors);

                $('tbody').append(tr);
            }
        }
    });

    $("#search").keyup(function() {
        var ketik = $("#search").val();
        console.log(ketik);

        //teknik ajax
        if(ketik.length){
            $.ajax({
                url: "story8/data?q=" + ketik,
                success: function(data){
                    // let bookList = $('tbody')
                    var array_items = data.items;
                    $('tbody').empty;
                    for (i=0; i<array_items.length; i++) {
                        var penulis = array_items[i].volumeInfo.authors;
                        var judul = array_items[i].volumeInfo.title;
                        var gambar = array_items[i].volumeInfo.imageLinks.smallThumbnail;

                        if(gambar == false){
                            var img = $('<td>').text("-");
                        }
                        else {
                            var img = $('<td>').append("<img src=" + gambar + ">");
                        }

                        var ttl = $('<td>').append(judul);

                        if(penulis == false) {
                            var aut = $('<td>').text("-");
                        }
                        else{
                            var aut = $('<td>').append(penulis);
                        }

                        var content_row = $('<tr>').append(img,ttl,aut);

                        $('tbody').append(content_row);
                    }
                }
            });
        }$("tbody").empty();
    });

});


   

